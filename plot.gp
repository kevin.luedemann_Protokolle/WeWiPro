reset

set terminal epslatex color
set key top left
f(x)=m*x+b
set xlabel "Kreisfrequenz $[\\text{kHz}]^2$"
set ylabel "Impedanz $[\\text{k}\\Omega]^2$"
set output 'Aus1.tex'
set fit logfile 'Aus1.log'
fit f(x) "daten_3.dat" u ((2*pi*$1/1000)**2):(($3/$5)**2):(sqrt($4**2*(2*$3/$5**2)**2+$6**2*(3*$3**2/$5**3)**2)) via m,b
p f(x) t "Regression", "daten_3.dat" u ((2*pi*$1/1000)**2):(($3/$5)**2):(sqrt($4**2*(2*$3/$5**2)**2+$6**2*(3*$3**2/$5**3)**2)) w e t "Messwerte"
set output
!epstopdf Aus1.eps

f(x)=atan((x*b-(1/(x*c)))/a)*180/pi
a=0.08
b=0.402
c=1.8
set xlabel "Kreisfrequenz [kHz]"
set ylabel "Phasenverschiebung [grad]"
set key center right
set output 'Aus3.tex'
set fit logfile 'Aus3.log'
fit f(x) "daten_4.dat" u ((2*pi*$1/1000)**2):10:11 via a,b,c
p "daten_3.dat" u ((2*pi*$1/1000)**2):7:8 w e t "Messwerte ohne C", "daten_4.dat" u ((2*pi*$1/1000)**2):10:11 w e t "Messwerte mit C", f(x) t "Fit Gleichung \\eqref{eq:phase}"
set output
!epstopdf Aus3.eps

set yrange [0:4]
#set xrange [1:1.5]
#f(x)=1/sqrt(1/sqrt(a**2+x**2*b**2)-x**2*c**2)
#f(x)=sqrt((a**2+(x*b)**2)/((x*c*a)**2+(1-x**2*b*c)**2))
f(x)=1/sqrt(((-(b*x)/(a**2+b**2*x**2)+(c*x))**2+(a/(a**2+b**2*x**2))**2))
#a=0.047
#b=2.2
#c=0.34
#f(x)=a*x**2+b*x+c
set ylabel "Impedanz [k$\\Omega$]"
set output 'Aus7.tex'
set fit logfile 'Aus7.log'
fit [1.1:1.35] f(x) "daten_5.dat" u (2*pi*$1/1000):($2/$4):(sqrt($3**2*(2*$2/$4**2)**2+$5**2*(3*$2**2/$4**3)**2)) via a,b,c
p "daten_5.dat" u (2*pi*$1/1000):($2/$4):(sqrt($3**2*(2*$2/$4**2)**2+$5**2*(3*$2**2/$4**3)**2)) w e t "Messwerte", f(x) t "Fit Gleichung \\eqref{eq:bezpara}"
set output
!epstopdf Aus7.eps

set ylabel "Spannung [V]"

set yrange [0:60]
#f(x)=a*x**2+b*x+c
#g(x)=h*x**2+j*x+k
set key top right
set output 'Aus5.tex'
#set fit logfile 'Aus5_l.log'
#fit [1:1.3] f(x) "daten_4.dat" u (2*pi*$1/1000):6:7 via a,b,c
#set fit logfile 'Aus5_c.log'
#fit [1.1:1.2] g(x) "daten_4.dat" u (2*pi*$1/1000):8:9 via h,j,k
p "daten_4.dat" u (2*pi*$1/1000):2:3 w e t "Messwerte U", "daten_4.dat" u (2*pi*$1/1000):6:7 w e t "Messwerte $U_\\text{L}$", "daten_4.dat" u (2*pi*$1/1000):8:9 w e t "Messwerte $U_\\text{C}$"#, f(x) t "quadratische Regression $U_L$", g(x) t "quadratische Regression $U_C$"
set output
!epstopdf Aus5.eps

set key top left
set yrange [:1]
f(x)=sqrt(a**2+(x*b-(1/(x*c)))**2)
a=0.08
b=0.5
c=2

set ylabel "Impedanz [k$\\Omega$]"
set output 'Aus2.tex'
set fit logfile 'Aus2.log'
fit [0.8:1.4] f(x) "daten_4.dat" u (2*pi*$1/1000):($2/$4):(sqrt($3**2*(2*$2/$4**2)**2+$5**2*(3*$2**2/$4**3)**2)) via a,b,c
p f(x) t "Fit Gleichung \\ref{eq:betZser}",  "daten_4.dat" u (2*pi*$1/1000):($2/$4):(sqrt($3**2*(2*$2/$4**2)**2+$5**2*(3*$2**2/$4**3)**2)) w e t "Messwerte"
set output
!epstopdf Aus2.eps
